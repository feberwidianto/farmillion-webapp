import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  NavbarText
} from "reactstrap";
import "./Header.css";

const Header = props => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className="container-header">
      <Navbar light expand="md" className="container-navbar">
        <NavbarBrand href="/home" style={{ color: "#A9C52F" }}>
          Farmillion
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem className="menubar">
              <NavLink
                href="/home/"
                className="menutext"
                style={{ color: "#A9C52F" }}
              >
                Home
              </NavLink>
            </NavItem>
            <NavItem className="menubar">
              <NavLink
                href="/agripreneur-kami/"
                className="menutext"
                style={{ color: "#A9C52F" }}
              >
                Agripreneur Kami
              </NavLink>
            </NavItem>
            <NavItem className="menubar">
              <NavLink
                href="/komoditi/"
                className="menutext"
                style={{ color: "#A9C52F" }}
              >
                {" "}
                Komoditi
              </NavLink>
            </NavItem>
            <NavItem className="menubar">
              <NavLink
                href="/panen/"
                className="menutext"
                style={{ color: "#A9C52F" }}
              >
                {" "}
                Panen
              </NavLink>
            </NavItem>
            <NavItem className="menubar">
              <NavLink
                href="/blog/"
                className="menutext"
                style={{ color: "#A9C52F" }}
              >
                {" "}
                Blog
              </NavLink>
            </NavItem>
            <NavItem className="menubar">
              <NavLink
                href="/app/"
                style={{ color: "#A9C52F", fontWeight: "bolder" }}
              >
                {" "}
                App
              </NavLink>
            </NavItem>
          </Nav>
          <NavbarText>
            <a href="/masuk">
              <Button className="btn-masuk" 
              >
                Masuk
              </Button>
            </a>
          </NavbarText>
        </Collapse>
      </Navbar>
    </div>
  );
};

export default Header;
