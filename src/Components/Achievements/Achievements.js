import React, { Component } from "react";
import { Card, CardTitle, CardText, Row, Col } from "reactstrap";
import "./Achievement.css";

class Achievements extends Component {
  render() {
    return (
      <div className="container-achievement">
        <div className="row-achievement">
          <Row>
            <Col sm="3">
              <Card body className="card-box">
                <CardTitle className="title-box">
                  <h1>10</h1>
                </CardTitle>
                <CardText className="subtitle-box">
                  <h6>Komoditi Pertanian</h6>
                </CardText>
              </Card>
            </Col>
            <Col sm="6">
              <Card body className="card-box-center">
                <CardTitle className="title-box">
                  <h1>1.000.000</h1>
                </CardTitle>
                <CardText className="subtitle-box">
                  <h6>Kilogram Panen</h6>
                </CardText>
              </Card>
            </Col>
            <Col sm="3">
              <Card body className="card-box">
                <CardTitle className="title-box">
                  <h1>100</h1>
                </CardTitle>
                <CardText className="subtitle-box">
                  <h6>Petani Indonesia</h6>
                </CardText>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Achievements;
