import React, { Component } from "react";
import { CardBody, CardTitle, CardText, Button } from "reactstrap";
import "./Banner.css";

class Banner extends Component {
  render() {
    return (
      <div className="container-banner">
        <img
          width="100%"
          height="100%"
          src="/Images/Banner.png"
          alt="background"
        />
        <CardBody className="content-banner">
          <CardBody width="100%">
            <CardTitle style={{ fontFamily: "Lobster" }}>
              <h1>Lahirkan Sejuta Petani Muda Indonesia</h1>
            </CardTitle>
            <CardText style={{ fontFamily: "Comfortaa" }}>
              <h5>
                Jadilah bagian dari gerakan sosial bangkitkan kembali pertanian
                Indonesia
              </h5>
            </CardText>
          </CardBody>

          <CardBody>
            <a href="/pelajari-lanjut">
              <Button className="button">Pelajari Lebih Lanjut</Button>
            </a>
          </CardBody>
        </CardBody>
      </div>
    );
  }
}

export default Banner;
