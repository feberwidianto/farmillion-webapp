import React, { Component } from 'react';
import Header from '../../Components/Header/Header'
import Banner from '../../Components/Banner/Banner';
import Achievements from '../../Components/Achievements/Achievements';

class Home extends Component {
    render(){
        return(
            <div>
                <Header/>
                <Banner/>
                <Achievements/>
            </div>
        )
    }
}

export default Home;